@extends('adminlte.master')
@section('content')
    <h1> Buat Account Baru! </h1>
    <h2> Sign Up Form </h2>
    <form action="/post" method="POST">
       @csrf
      <label for="fname">First Name</label> <br>
      <input type="text" name="first_name" id="fname">
     <br><br>
      <label for="lname">Last Name</label> <br>
      <input type="text" name="last_name" id="lname">
      
     <br>

    <label>Gender</label> <br><br>
    <input type="radio" name="gender" value="0" checked>Male <br>
    <input type="radio" name="gender" value="1">Female <br>
    <input type="radio" name="gender" value="2">Other <br><br>

    <label>Nationality</label> <br><br>
    <select>
      <option>Indonesia</option>
      <option>Malaysia</option>
      <option>Namec</option>
      <option>Klingon</option>
      </select>
      <br><br>

      <label>Language Spoken</label> <br><br>
      <input type="checkbox" value="0"> Bahasa Indonesia <br>
      <input type="checkbox" value="1"> English <br>
      <input type="checkbox" value="2"> Other <br><br>
      <br><br>
       <label>Bio</label> <br><br>
       <textarea cols="40" rows="8"></textarea>
       <br>
      
    
    <input type="submit" value="Sign Up">
    
    
    
    </form>
@endsection
