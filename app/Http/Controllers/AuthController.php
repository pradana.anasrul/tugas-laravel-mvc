<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function post(Request $request)
    {
        //dd($request);
       $fname = $request->first_name;
       $lname = $request->last_name;

       return view('post', compact('fname','lname'));
    }

    
}