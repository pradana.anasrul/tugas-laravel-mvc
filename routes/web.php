<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/index', function(){
//    return view('index');
//});

//Route::get('/', function(){
//    return view('blank');
//});

//Route::get('/data-table', function(){
 //   return view('data-tables');
//});

//Route::get('/form', function(){
//    return view('form');
//});

//Route::get('/index', 'HomeController@index');
//Route::get('/form', 'AuthController@form');
//Route::post('/post', 'AuthController@post');

/*Route::get('/master',function(){
    return view('adminlte.master');
});
Route::get('/items', function(){
    return view('items.index');
});

Route::get('/items/create',function() {
    return view('items.create');
});*/

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');